import time, csv
from PyQt5.QtCore import Qt, QFileInfo

wd = '/home/gabor/Documents/milvus/atlasz/breeding-bird-atlas/qgis-projects/'

def removeGroup(name):
    root = QgsProject.instance().layerTreeRoot()
    group = root.findGroup(name)
    if not group is None:
        for child in group.children():
            dump = child.dump()
            id = dump.split("=")[-1].strip()
            QgsMapLayerRegistry.instance().removeMapLayer(id)
        root.removeChildNode(group)

csal = [
'GALLIFORMES: Phasianidae',
'ANSERIFORMES: Anatidae',
'PODICIPEDIFORMES: Podicipedidae',
'COLUMBIFORMES: Columbidae',
'CAPRIMULGIFORMES: Caprimulgidae',
'CAPRIMULGIFORMES: Apodidae',
'CUCULIFORMES: Cuculidae',
'GRUIFORMES: Rallidae',
'OTIDIFORMES: Otididae',
'CICONIIFORMES: Ciconiidae',
'PELECANIFORMES: Threskiornithidae',
'PELECANIFORMES: Ardeidae',
'PELECANIFORMES: Pelecanidae',
'SULIFORMES: Phalacrocoracidae',
'CHARADRIIFORMES: Burhinidae',
'CHARADRIIFORMES: Haematopodidae',
'CHARADRIIFORMES: Recurvirostridae',
'CHARADRIIFORMES: Charadriidae',
'CHARADRIIFORMES: Scolopacidae',
'CHARADRIIFORMES: Glareolidae',
'CHARADRIIFORMES: Laridae',
'STRIGIFORMES: Tytonidae',
'STRIGIFORMES: Strigidae',
'ACCIPITRIFORMES: Accipitridae',
'BUCEROTIFORMES: Upupidae',
'CORACIIFORMES: Meropidae',
'CORACIIFORMES: Coraciidae',
'CORACIIFORMES: Alcedinidae',
'PICIFORMES: Picidae',
'FALCONIFORMES: Falconidae',
'PASSERIFORMES: Laniidae',
'PASSERIFORMES: Oriolidae',
'PASSERIFORMES: Corvidae',
'PASSERIFORMES: Paridae',
'PASSERIFORMES: Remizidae',
'PASSERIFORMES: Hirundinidae',
'PASSERIFORMES: Aegithalidae',
'PASSERIFORMES: Alaudidae',
'PASSERIFORMES: Sylviidae',
'PASSERIFORMES: Timaliidae',
'PASSERIFORMES: Reguliidae',
'PASSERIFORMES: Troglodytidae',
'PASSERIFORMES: Sittidae',
'PASSERIFORMES: Certhiidae',
'PASSERIFORMES: Sturnidae',
'PASSERIFORMES: Turdidae',
'PASSERIFORMES: Muscicapidae',
'PASSERIFORMES: Cinclidae',
'PASSERIFORMES: Passeridae',
'PASSERIFORMES: Prunellidae',
'PASSERIFORMES: Motacillidae',
'PASSERIFORMES: Fringillidae',
'PASSERIFORMES: Emberizidae',
'PSITTACIFORMES: Psittacidae'
]

sps = [
['Coturnix coturnix','Phasianus colchicus','Perdix perdix','Bonasa bonasia','Tetrao urogallus','Tetrao tetrix'],
['Anas acuta','Anas clypeata','Anas crecca','Anas penelope','Anas platyrhynchos','Anas querquedula','Anas strepera','Anser anser','Aythya ferina','Aythya fuligula','Aythya nyroca','Bucephala clangula','Cygnus olor','Mergellus albellus','Mergus merganser','Netta rufina','Tadorna ferruginea','Tadorna tadorna','Aix galericulata','Aix sponsa'],
['Tachybaptus ruficollis','Podiceps grisegena','Podiceps cristatus','Podiceps nigricollis'],
['Columba livia domestica','Columba oenas','Columba palumbus','Streptopelia turtur','Streptopelia decaocto'],
['Caprimulgus europaeus'],
['Tachymarptis melba','Apus pallidus','Apus apus'],
['Cuculus canorus'],
['Rallus aquaticus','Crex crex','Porzana porzana','Porzana parva','Porzana pusilla','Gallinula chloropus','Fulica atra'],
['Otis tarda'],
['Ciconia nigra','Ciconia ciconia'],
['Platalea leucorodia','Plegadis falcinellus'],
['Botaurus stellaris','Ixobrychus minutus','Nycticorax nycticorax','Ardeola ralloides','Bubulcus ibis','Ardea cinerea','Ardea purpurea','Egretta alba','Egretta garzetta'],
['Pelecanus crispus','Pelecanus onocrotalus'],
['Microcarbo pygmeus','Phalacrocorax carbo'],
['Burhinus oedicnemus'],
['Haematopus ostralegus'],
['Recurvirostra avosetta','Himantopus himantopus'],
['Eudromias morinellus','Charadrius dubius','Charadrius alexandrinus','Vanellus vanellus'],
['Numenius arquata','Limosa limosa','Scolopax rusticola','Gallinago gallinago','Actitis hypoleucos','Tringa ochropus','Tringa totanus','Tringa stagnatilis'],
['Glareola pratincola','Glareola nordmanni'],
['Larus ridibundus','Larus ichthyaetus','Larus melanocephalus','Larus michahellis','Larus canus','Larus cachinnans','Larus cachinnans michahellis','Sternula albifrons','Gelochelidon nilotica','Chlidonias hybrida','Chlidonias leucopterus','Chlidonias niger','Sterna hirundo','Thalasseus sandvicensis'],
['Tyto alba'],
['Glaucidium passerinum','Athene noctua','Aegolius funereus','Otus scops','Asio otus','Asio flammeus','Strix aluco','Strix uralensis','Bubo bubo'],
['Accipiter brevipes','Accipiter gentilis','Accipiter nisus','Aquila chrysaetos','Aquila heliaca','Aquila pomarina','Buteo buteo','Buteo rufinus','Circaetus gallicus','Circus aeruginosus','Circus pygargus','Haliaeetus albicilla','Hieraaetus pennatus','Milvus migrans','Pernis apivorus'],
['Upupa epops'],
['Merops apiaster'],
['Coracias garrulus'],
['Alcedo atthis'],
['Jynx torquilla','Picus canus','Picus viridis','Dryocopus martius','Picoides tridactylus','Dendrocopos medius','Dendrocopos minor','Dendrocopos leucotos','Dendrocopos syriacus','Dendrocopos major'],
['Falco tinnunculus','Falco vespertinus','Falco subbuteo','Falco cherrug','Falco peregrinus'],
['Lanius collurio','Lanius minor','Lanius excubitor','Lanius senator'],
['Oriolus oriolus'],
['Garrulus glandarius','Pica pica','Nucifraga caryocatactes','Corvus monedula','Corvus frugilegus','Corvus corone cornix','Corvus corax'],
['Parus palustris','Parus lugubris','Parus montanus','Parus ater','Parus cristatus','Parus major','Parus caeruleus'],
['Remiz pendulinus'],
['Riparia riparia','Ptyonoprogne rupestris','Hirundo rustica','Hirundo daurica','Delichon urbicum'],
['Aegithalos caudatus'],
['Melanocorypha calandra','Calandrella brachydactyla','Galerida cristata','Lullula arborea','Alauda arvensis','Eremophila alpestris'],
['Cettia cetti','Locustella naevia','Locustella fluviatilis','Locustella luscinioides','Acrocephalus melanopogon','Acrocephalus schoenobaenus','Acrocephalus agricola','Acrocephalus scirpaceus','Acrocephalus palustris','Acrocephalus arundinaceus','Hippolais pallida','Hippolais icterina','Phylloscopus trochilus','Phylloscopus collybita','Phylloscopus sibilatrix','Sylvia atricapilla','Sylvia borin','Sylvia communis','Sylvia curruca','Sylvia nisoria'],
['Panurus biarmicus'],
['Regulus regulus','Regulus ignicapilla'],
['Troglodytes troglodytes'],
['Sitta europaea','Tichodroma muraria'],
['Certhia familiaris','Certhia brachydactyla'],
['Sturnus roseus','Sturnus vulgaris'],
['Turdus torquatus','Turdus merula','Turdus pilaris','Turdus philomelos','Turdus viscivorus'],
['Erithacus rubecula','Luscinia luscinia','Luscinia megarhynchos','Luscinia svecica','Phoenicurus ochruros','Phoenicurus phoenicurus','Saxicola rubetra','Saxicola torquatus','Oenanthe oenanthe','Oenanthe hispanica','Oenanthe pleschanka','Oenanthe isabellina','Monticola saxatilis','Muscicapa striata','Ficedula hypoleuca','Ficedula albicollis','Ficedula semitorquata','Ficedula parva'],
['Cinclus cinclus'],
['Passer domesticus','Passer hispaniolensis','Passer montanus'],
['Prunella collaris','Prunella modularis'],
['Motacilla alba','Motacilla citreola','Motacilla flava','Motacilla cinerea','Anthus campestris','Anthus trivialis','Anthus pratensis','Anthus spinoletta'],
['Fringilla coelebs','Serinus serinus','Carduelis chloris','Carduelis spinus','Carduelis carduelis','Carduelis flammea','Carduelis cannabina','Carpodacus erythrinus','Loxia curvirostra','Pyrrhula pyrrhula','Coccothraustes coccothraustes'],
['Miliaria calandra','Emberiza citrinella','Emberiza cirlus','Emberiza cia','Emberiza hortulana','Emberiza melanocephala','Emberiza schoeniclus'],
['Psittacula krameri']
]

project = QgsProject.instance()
root = project.layerTreeRoot()
layers = {}
for i in range(len(csal)):
    root.addGroup(csal[i])
    parent = root.findGroup(csal[i])
    uri = QgsDataSourceUri()
    uri.setConnection("10.8.83.1","5432","atlasdata","qgis_web","s6tWXkYS")

    for sp in sps[i]:
        wms_ab = ''
        wms_pa = ''
        rlayer = ''
        rlayer2 = ''
        with open(wd + 'model_names.csv',"r") as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                if (sp == row['species']) and (row['type'] == 'abundance'):
                    wms_ab = row
                if (sp == row['species']) and (row['type'] == 'presence_absence'):
                    wms_pa = row
            if wms_ab:
                    
                uri_separator = '&'
                uri_url = 'url=http://harta.atlaspasari.ro/qgis/qgis_mapserv.fcgi?map=/var/www/html/qgis/projects/' + wms_ab['wms']
                uri_username = 'username=user'
                uri_password = 'password=pass'
                uri_format = 'format=image/png'
                uri_layers = 'layers=' + wms_ab['model_name']
                uri_crs = 'crs=EPSG:4326'
                uri_styles = 'styles&'

                url_with_params = uri_separator.join((uri_url,
                                                      uri_username,
                                                      uri_password,
                                                      uri_format,
                                                      uri_layers,
                                                      uri_crs,
                                                      uri_styles))

                rlayer = QgsRasterLayer(url_with_params, 'abundance ' + wms_ab['species'], 'wms')
                if not rlayer.isValid():
                    print('hiba')
            if wms_pa:
                    
                uri_separator = '&'
                uri_url = 'url=http://harta.atlaspasari.ro/qgis/qgis_mapserv.fcgi?map=/var/www/html/qgis/projects/presence_absence.qgs'
                uri_username = 'username=user'
                uri_password = 'password=pass'
                uri_format = 'format=image/png'
                uri_layers = 'layers=' + wms_pa['model_name']
                uri_crs = 'crs=EPSG:4326'
                uri_styles = 'styles&'

                url_with_params = uri_separator.join((uri_url,
                                                      uri_username,
                                                      uri_password,
                                                      uri_format,
                                                      uri_layers,
                                                      uri_crs,
                                                      uri_styles))

                rlayer2 = QgsRasterLayer(url_with_params, 'presence_absence ' + wms_pa['species'], 'wms')
                if not rlayer2.isValid():
                    print('hiba presence absence')
            
        sp_layer = parent.addGroup(sp)
        uri.setDataSource("public", sp.lower().replace(" ","_"), "geometrie", "", "")
        grid_layer = QgsVectorLayer(uri.uri(), "grid "+sp, "postgres")
        uri.setDataSource("public", "birddata", "geometrie", "specia='"+sp+"' AND GeometryType(geometrie) = 'POINT' AND lp = TRUE", "gid")
        point_layer = QgsVectorLayer(uri.uri(), "point "+sp, "postgres")
        uri.setDataSource("public", "birddata", "geometrie", "specia='"+sp+"' AND GeometryType(geometrie) = 'LINESTRING' AND lp = TRUE", "gid")
        line_layer = QgsVectorLayer(uri.uri(), "line "+sp, "postgres")
        uri.setDataSource("public", "birddata", "geometrie", "specia='"+sp+"' AND GeometryType(geometrie) = 'POLYGON' AND lp = TRUE", "gid")
        polygon_layer = QgsVectorLayer(uri.uri(), "polygon "+sp, "postgres")
        
        if not grid_layer.isValid():
            print("Layer {0:s} did not load".format(grid_layer.name()))
        if not point_layer.isValid():
            print("Layer {0:s} did not load".format(point_layer.name()))
        if not line_layer.isValid():
            print("Layer {0:s} did not load".format(line_layer.name()))
        if not polygon_layer.isValid():
            print("Layer {0:s} did not load".format(polygon_layer.name()))

        layers[grid_layer.name()] = grid_layer
        layers[point_layer.name()] = point_layer
        layers[line_layer.name()] = line_layer
        layers[polygon_layer.name()] = polygon_layer
        
        if rlayer != '' and rlayer2 != '':
            layers[rlayer.name()] = rlayer
            layers[rlayer2.name()] = rlayer2
            QgsMapLayerRegistry.instance().addMapLayers([layers["grid "+sp],
            layers["point "+sp],layers["line "+sp],layers["polygon "+sp],layers["abundance "+sp],layers["presence_absence "+sp]],False)
        elif (rlayer != '') and (rlayer2 == ''):    
            layers[rlayer.name()] = rlayer
            QgsMapLayerRegistry.instance().addMapLayers([layers["grid "+sp],
            layers["point "+sp],layers["line "+sp],layers["polygon "+sp],layers["abundance "+sp]],False)
        elif rlayer == '' and rlayer2 != '':
            layers[rlayer2.name()] = rlayer2
            QgsMapLayerRegistry.instance().addMapLayers([layers["grid "+sp],
            layers["point "+sp],layers["line "+sp],layers["polygon "+sp],layers["presence_absence "+sp]],False)
        else:
            QgsMapLayerRegistry.instance().addMapLayers([layers["grid "+sp],
            layers["point "+sp],layers["line "+sp],layers["polygon "+sp]],False)
        
        sp_layer.addLayer(layers["grid "+sp])
        sp_layer.addLayer(layers["point "+sp])
        sp_layer.addLayer(layers["line "+sp])
        sp_layer.addLayer(layers["polygon "+sp])
        
        if rlayer != '':
            sp_layer.addLayer(layers["abundance "+sp])
        
        if rlayer2 != '':
            sp_layer.addLayer(layers["presence_absence "+sp])
    
    parent.setExpanded(False)
    uri.setDataSource("public", "grid_10km", "geometrie", "", "cellcode")
    resp_layer = QgsVectorLayer(uri.uri(), "responsabili", "postgres")
    if not resp_layer.isValid():
        print("Layer {0:s} did not load".format(resp_layer.name()))
    
    QgsMapLayerRegistry.instance().addMapLayer(resp_layer,False)
    root.addLayer(resp_layer)
    root.setExpanded(False)
    root.setVisible(Qt.Unchecked)
    project.write(QFileInfo('projects/' + csal[i].lower().replace(": ","-") + '.qgs'))    
  #  project.write(QFileInfo('/home/gabor/Documents/milvus/atlasz/qgis_projektek/' + csal[i].lower().replace(": ","-") + '.qgs'))
    QgsMapLayerRegistry.instance().removeMapLayer(resp_layer.id())
    removeGroup(csal[i])
