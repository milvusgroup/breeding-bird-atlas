from osgeo import gdal
import time, csv

wd = '/home/gabor/Documents/milvus/atlasz/breeding-bird-atlas/qgis-projects/'


mapfile = """MAP
    NAME "atlas-models-map"
    SIZE 250 250 
    STATUS ON
    UNITS DD

    EXTENT 20.3155848885023 43.6498278012919 29.8736903559214 48.3006906027268

    DEBUG 0
    CONFIG MS_ERRORFILE "/tmp/atlas_ms_error.txt"
    CONFIG MS_ENCRYPTION_KEY "/var/lib/openbiomaps/data/maps/access.key"
    SHAPEPATH "/var/lib/openbiomaps/data/maps/"
    SYMBOLSET "/var/lib/openbiomaps/data/maps/symbols.txt"
    IMAGEQUALITY 95
    IMAGETYPE agg 

    PROJECTION
        "init=epsg:3857"
    END 

    WEB 
        IMAGEPATH "/tmp/"
        IMAGEURL "/tmp/"
        TEMPLATE "/var/lib/openbiomaps/data/maps/empty.html"
        METADATA
            "wfs_name"           "atlas"
            "ows_name"           "atlas"
            "wms_name"           "atlas"
            "wfs_title"          "atlas"
            "wms_title"          "atlas"
            "ows_title"          "atlas"
            "wfs_enable_request"  "*" 
            "wms_enable_request"  "*" 
            "ows_enable_request"  "*" 
            "wfs_encoding"        "UTF-8"
            "wms_encoding"        "UTF-8"
            "ows_encoding"        "UTF-8"
        END    
    END 

    OUTPUTFORMAT
        NAME agg 
        DRIVER agg/png
        IMAGEMODE rgb 
    END
    """

with open(wd + 'model_names.csv',"r") as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        #print(row['species'])

        # open raster and choose band to find min, max
        raster = wd + row['file']
        gtif = gdal.Open(raster)
        srcband = gtif.GetRasterBand(1)

        #Get raster statistics
        stats = srcband.GetStatistics(True, True)

        mapfile = mapfile + """
    LAYER
        NAME "{0} {4}"
        STATUS ON
        TYPE RASTER
        PROJECTION
            "init=epsg:31700"
        END 
        DATA "/var/www/html/qgis/projects/{1}"
        CLASSITEM "[pixel]"
        CLASS
          EXPRESSION ([pixel] >= {2} AND [pixel] <= {3})  
          STYLE 
            COLORRANGE 255 255 0 255 0 0   #color start RGB and end RGB 
            DATARANGE {2} {3}
            RANGEITEM "pixel"
          END
        END 
    END
""".format(row['species'], row['file'], stats[0], stats[1], row['type'])
mapfile = mapfile + 'END'
with open("models.map", "w") as text_file:
    text_file.write(mapfile)

