BEGIN;
    DROP TABLE birddata_observers;
    DROP TABLE birddata_observers_connect;
COMMIT;

BEGIN;
    CREATE TABLE birddata_observers (
        uid serial PRIMARY KEY,
        observer_id integer,
        name character varying,
        correct_name character varying,
        status character varying DEFAULT 'unknown');
    CREATE TABLE birddata_observers_connect (
        row_id integer,
        observer_id integer
    );
    GRANT ALL ON birddata_observers TO qgis_web;
    GRANT ALL ON birddata_observers_connect TO qgis_web;

    INSERT INTO birddata_observers (observer_id, name) 
        SELECT row_number() OVER ( ORDER BY observator ) as n, observator FROM ( 
            SELECT DISTINCT trim(unnest(regexp_split_to_array(observator,E'[,;]\\s*'))) as observator FROM birddata) AS foo;

    INSERT INTO birddata_observers_connect (row_id, observer_id)  
        SELECT gid, bo.observer_id FROM (
            SELECT gid, trim(unnest(regexp_split_to_array(observator,E'[,;]\\s*'))) as observator FROM birddata) foo  
            LEFT JOIN birddata_observers bo ON foo.observator = bo.name;

COMMIT;


BEGIN;

    CREATE OR REPLACE FUNCTION "connect_atlas_observers" (accepted integer, ids integer[] DEFAULT '{}') RETURNS VOID AS $$
    BEGIN
        UPDATE birddata_observers SET status = 'accepted', correct_name = name WHERE observer_id = accepted AND status = 'unknown';
        UPDATE birddata_observers SET status = 'misspelled', observer_id = accepted WHERE observer_id = ANY (ids::int[]);
        UPDATE birddata_observers_connect SET observer_id = accepted WHERE observer_id = ANY (ids::int[]);
    END $$ LANGUAGE plpgsql;

COMMIT;
